import Redis from 'redis'
import { Colorize } from '../utils/logger'
import LoopNext from 'loopnext'
var Iterate = new LoopNext().syncLoop

export default function (config) {
  return new Promise((resolve) => {
    let Logger = new Colorize(config)

    let redisConf = config.redis
    let connections = Object.keys(redisConf)
    var createConnections = {}

    var counter = 0
    Iterate(connections.length, function (loop) {
      var conName = connections[counter]
      let singleConf = redisConf[conName]

      const client = Redis.createClient(
        singleConf.port
        , singleConf.host
        , { no_ready_check: true }
      )

      client.select(singleConf.db, function (err, reply) {
        Logger.success('Redis Connection : ' + conName + ' = Database ' + singleConf.db + ' selected')
        createConnections[conName] = client
        counter++
        loop.next()
      })

      client.on("error", function (err) {
        Logger.error('Redis Connection: ' + conName + ' = Redis Transaction Error : ' + err)
        createConnections[conName] = 'Redis Connection : ' + conName + ' = Redis Transaction Error : ' + err
        counter++
        loop.next()
      })

    }, function () {
      resolve(createConnections)
    console.log('\n\n\n----------------------------------------------------------------\n')
    Logger.success('Ready to accept requests')
    })

  })

}
