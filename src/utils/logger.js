import ColorizeLog from 'colorize-log'
import winston from 'winston'
import { getTimestamp } from './dates'


export var Colorize = function (config) {
  let defaults = {
    levels: ['start', 'info', 'warn', 'error', 'success', 'data', 'header', 'url', 'endpoint','status'],
    colors: {
      start: 'cyan',
      info: 'gray',
      warn: 'yellow',
      error: 'red',
      success: 'green',
      data: 'magenta',
      header: 'gray',
      url: 'blue',
      endpoint: 'green',
      status: 'cyan',
    },
    consoleColor: 'reset',
    showLevels: true,
    levelsCase: 'uppercase',
    colorizeLevels: false
  }

  const LogConf = config.logger
  if (!LogConf) LogConf = defaults
  //levels
  if (LogConf.levels.indexOf('start') < 0) LogConf.levels.push('start')
  if (LogConf.levels.indexOf('info') < 0) LogConf.levels.push('info')
  if (LogConf.levels.indexOf('warn') < 0) LogConf.levels.push('warn')
  if (LogConf.levels.indexOf('error') < 0) LogConf.levels.push('error')
  if (LogConf.levels.indexOf('success') < 0) LogConf.levels.push('success')
  if (LogConf.levels.indexOf('data') < 0) LogConf.levels.push('data')
  if (LogConf.levels.indexOf('header') < 0) LogConf.levels.push('header')
  if (LogConf.levels.indexOf('url') < 0) LogConf.levels.push('url')
  if (LogConf.levels.indexOf('endpoint') < 0) LogConf.levels.push('endpoint')
  if (LogConf.levels.indexOf('status') < 0) LogConf.levels.push('status')
  //colors
  if (!LogConf.colors.start) LogConf.colors.start = 'cyan'
  if (!LogConf.colors.info) LogConf.colors.info = 'gray'
  if (!LogConf.colors.warn) LogConf.colors.warn = 'yellow'
  if (!LogConf.colors.error) LogConf.colors.error = 'red'
  if (!LogConf.colors.success) LogConf.colors.success = 'green'
  if (!LogConf.colors.data) LogConf.colors.data = 'magenta'
  if (!LogConf.colors.header) LogConf.colors.header = 'gray'
  if (!LogConf.colors.url) LogConf.colors.url = 'blue'
  if (!LogConf.colors.endpoint) LogConf.colors.endpoint = 'green'
  if (!LogConf.colors.status) LogConf.colors.status = 'cyan'

  return new ColorizeLog(LogConf)

}

export var External_Logger = function (config) {

  var myCustomLevels = {
    levels: {
      info: 0,
      success: 1,
      warn: 2,
      error: 3
    },
    colors: {
      info: 'blue',
      success: 'green',
      warn: 'yellow',
      error: 'red'
    }
  }

  // winston.addColors(myCustomLevels.colors)

  if (!config.logger.rotate) {

    return new (winston.Logger)(
      {
        levels: {
          trace: 0,
          input: 1,
          verbose: 2,
          prompt: 3,
          debug: 4,
          info: 5,
          data: 6,
          help: 7,
          warn: 8,
          error: 9
        },
        colors: {
          trace: 'magenta',
          input: 'grey',
          verbose: 'cyan',
          prompt: 'grey',
          debug: 'blue',
          info: 'green',
          data: 'grey',
          help: 'cyan',
          warn: 'yellow',
          error: 'red'
        },
        transports: [
          new (winston.transports.Console)({ colorize: true })
        ]

      }
    )

  } else {

    return new (winston.Logger)({

      transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({ filename: config.logger.folder + getTimestamp().date + '_debug.log' })
      ]

    });

  }

}

export const Logger = new (winston.Logger)({

  transports: [
    new (winston.transports.Console)()
  ]

});



