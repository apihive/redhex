# redhex [API Builder]

A Redis(optional), Http/Https and Express API builder

### What's New
* Multiple Redis DB Connection (Starting version 1.0.35 up only)
* redis field is optional
* Log CustOm Colors
* Cors Configuration
* allowed uploading of file

#### Example of Multiple redis connection (OPTIONAL)
```javascript
 redis: { //opitonal
    read: {
      port: 6379,
      host: '127.0.0.1',
      db: 4
    },
    write: {
      port: 6379,
      host: '127.0.0.1',
      db: 5
    }
  }
```
Then user store like so
```javascript
const APP = new HEX(manifest)
  APP.Store.read.HGETALL('KEY', callback)
  APP.Store.write.HSET('KEY', ['NAME', 'NICK'], callback)
```

#### Example UPLOAD Config (OPTIONAL)
```javascript
  upload: { 
    folder: '/Users/username/path/to/folder/', //DEFAULTS './uploads'
    publicRoute: '/myroute', // DEFAULT '/uploads'
  }
```
Then in your request, add the following (important)
```
in formdata, key should be "filename"
in headers, add "enctype":"multipart/form-data"
```

then access uploaded files via http://localhost: port/myroute/filename.ext

#### Example CORS Config
```javascript
 cors: {
    origin: '*',
    methods: 'GET,PUT,POST,DELETE',
    headers: 'Content-Type,slave',
    maxAge: 5000,
    exposedHeaders: 'slave',
    credentials: false,
  }
```
and
#### Example Logger Config
Refer to [colorize-log](https://www.npmjs.com/package/colorize-log) module in npm
```javascript
  logger: {
    levels: ['start', 'info', 'warn', 'error', 'success', 'data', 'header', 'url'],
    colors: {
      start: 'cyan',
      info: 'gray',
      warn: 'yellow',
      error: 'red',
      success: 'green',
      data: 'magenta',
      header: 'gray',
      url: 'blue',
    },
    consoleColor: 'reset',
    showLevels: true,
    levelsCase: 'uppercase',
    colorizeLevels: false,
    folder: '/Users/username/path/to/folder/'
  }
```

### Installation
--save will save redhex to your dependencies
```
npm install redhex --save
```
### Manifest [config]
this can be any filename that contains your configurations
```javascript
{
  ssl:true, //dafault false
  keys:{
    privateKey: PATH.TO.SSL.KEY,
    certificate: PATH.TO.SSL.CERTIFICATE
  },
   cors: {
    origin: '*',
    methods: 'GET,PUT,POST,DELETE',
    headers: 'Content-Type,slave',
    maxAge: 5000,
    exposedHeaders: 'slave',
    credentials: false,
  },
  ports:{
    http:6001,
    https:7001
  },
  api:{
    prefix:ROUTE.PREFIX, //EX: 127.0.0.1:7001/[prexif]/endpoint
  },
  redis:{ // OPTIONAL
    port: 6379,
    host: '127.0.0.1',
    db:4
  },
  logger: {
    levels: ['start', 'info', 'warn', 'error', 'success', 'data', 'header', 'url'],
    colors: {
      start: 'cyan',
      info: 'gray',
      warn: 'yellow',
      error: 'red',
      success: 'green',
      data: 'magenta',
      header: 'gray',
      url: 'blue',
    },
    consoleColor: 'reset',
    showLevels: true,
    levelsCase: 'uppercase',
    colorizeLevels: false,
    folder: '/Users/username/path/to/folder/'
  },
  upload: {  // OPTIONAL
    folder: '/Users/username/path/to/folder/', //DEFAULTS './uploads'
    publicRoute: '/myroute', // DEFAULT '/uploads'
  }
}
```

### Usage
This is how you will use it in its simplest form.
```javascript
	//ES6
	import HEX FROM 'redhex'
    import manifest from '../path/to/manifest'
    const APP = new HEX(manifest)

    //CommonJS
    var HEX = require('redhex')
    var manifest = require('path/to/manifest')
    //or   
    var manifest = {//same a above manifest example}
    var APP = new HEX(manifest)
```
`Thus`
```javascript
    const APP = new HEX(manifest)
    //Returns Obj = {Route,Log,Store}
```
##### APP.Route([array])
Now supports Multipart uploading
```javascript
    const operatorFunc = function(req,res){}
    const endpoints = [
    					// Normal Route
                        {
                          url:'/user',
                          methods:['POST'],
                          operator: operatorFunc
                        },
                        // File Upload Route [Multipart]
                        {
                          url:'/upload',
                          methods:['POST'],
                          type:'multipart',
                          operator: operatorFunc
                        }
                      ]
    APP.Route(endpoints)
```
##### APP.Store (Optional)
```javascript
	const Store = APP.Store //SHORTEN [optional]
    Store.LPUSH(key, value, callback)
```

### Complete Todo Example
[REDHEX-TODO](https://github.com/apihive/redhex-todo) is on Github, Feel free to download it for free. Cheers!
```
https://github.com/apihive/redhex-todo
```
### Become a contributor
email me at
```
rhomnickcoloma@gmail.com
```

### ISC LICENSE (ISC)

Copyright (c) 2016, Rhomnick B. Coloma rhomnickcoloma@gmail.com

Permission to use, copy, modify, and/or distribute this software for any
with or without fee is hereby granted, provided that the above copyright
notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
