var HEX = require('../app')

var manifest = {
  ssl: false,
  keys: {
    privateKey: './keys/key.pem',
    certificate: './keys/server.crt'
  },
  cors: {
    origin: '*',
    methods: 'GET,PUT,POST,DELETE',
    headers: 'Content-Type,slaveS',
    maxAge: 5000,
    exposedHeaders: 'slaveS',
    credentials: false,
  },
  ports: {
    http: 2000,
    https: 2000
  },
  api: {
    prefix: '/test',
  },
  redis: { //opitonal
    read: {
      port: 6379,
      host: '127.0.0.1',
      db: 4
    },
    write: {
      port: 6379,
      host: '127.0.0.1',
      db: 5
    }
  },
  logger: {
    levels: ['start', 'info', 'warn', 'error', 'success', 'data', 'header', 'url'],
    colors: {
      start: 'cyan',
      info: 'gray',
      warn: 'yellow',
      error: 'red',
      success: 'green',
      data: 'magenta',
      header: 'gray',
      url: 'blue',
    },
    consoleColor: 'reset',
    showLevels: true,
    levelsCase: 'uppercase',
    colorizeLevels: false,
    folder: '/Users/nickolaj/Documents/PROJECTS/NPM-PACKAGES/redhex/test/logs/',
    upload: {
      folder: '/Users/nickolaj/Documents/PROJECTS/NPM-PACKAGES/redhex/test/uploads/'
    }
  }
}

var Server = new HEX(manifest)


var userFunc = function (req, res) {
  console.log(req.body)
}

var uploadFunc = function (req, res) {
  console.log('FILE', 'Name: ' + req.file.originalname + ' Size: ' + req.file.size)
}

Server.Route([
  // Normal Route 
  {
    url: '/get',
    methods: ['GET'],
    middleware: [
      function (req, res, next) {
        console.log('Getting PO')
        next()
      },
      function (req, res, next) {
        console.log('Getting PO2')
        next()
      },
      function (req, res, next) {
        console.log('Getting PO3')
        next()
      }
    ],
    operator: userFunc
  },
  {
    url: '/post',
    methods: ['DELETE'],
    operator: userFunc
  },
  // File Upload Route [Multipart] 
  {
    url: '/upload',
    methods: ['POST'],
    type: 'multipart',
    operator: uploadFunc
  }
])

Server.Log.start('alpit test')
Server.Log.warn('alpit test')
Server.Log.info('alpit test')
Server.Log.success('alpit test')
Server.Log.data('alpit test')
Server.Log.header('alpit test')
Server.Log.url('alpit test')

console.log(Server)
var counter = 0

Server.Log.start('Iteration Start')

Server.Iterate(5, function (loop) {

  Server.Log.info(counter)

  counter++

  loop.next()

}, function () {
  Server.Log.success('Iteration Success')
})
// setTimeout(function () {
//   Server.Store.write.HSET('USERS', ['NAME', 'NICK'], function (err, reply) {
//     console.log(err, reply)
//   })
//   Server.Store.read.HSET('USERS', ['NAME', 'BAYA'], function (err, reply) {
//     console.log(err, reply)
//   })
// }, 2000)