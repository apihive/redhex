/* @flow weak */
import express from 'express'
import bodyParser from 'body-parser'
import multer from 'multer'
import morgan from 'morgan'
import HTTP from 'http'
import HTTPS from 'https'
import fs from 'fs'
import {
  Colorize
} from '../utils/logger'
import path from 'path'
import crypto from 'crypto'



const Server = function (config) {

  var Cors = config.cors

  let Logger = new Colorize(config)

  var publicFolder = './uploads'

  var publicRoute = '/uploads'

  if (config.upload) {
    if (config.upload.folder) publicFolder = config.upload.folder
    if (config.upload.publicRoute) publicRoute = config.upload.publicRoute
  }

  var storage = multer.diskStorage({
    destination: publicFolder,
    filename: function (req, file, cb) {
      crypto.pseudoRandomBytes(16, function (err, raw) {
        if (err) return cb(err)
        cb(null, raw.toString('hex') + path.extname(file.originalname))
      })
    }
  })

  var upload = multer({
    storage: storage
  });

  const app = express()

  const router = express.Router()

  router.use(function (req, res, next) {

    Logger.url(`[ ${req.method} ] REQUEST VIA : ${config.api.prefix}${req.url}`)

    if (!Cors) return next()

    if (Cors.origin && Cors.origin != '') {
      res.header('Access-Control-Allow-Origin', Cors.origin)
    }
    if (Cors.methods && Cors.methods != '') {
      res.header('Access-Control-Allow-Methods', Cors.methods)
    }
    if (Cors.headers && Cors.headers != '') {
      res.header('Access-Control-Allow-Headers', Cors.headers)
    }
    if (Cors.maxAge && Cors.maxAge != '') {
      res.header('Access-Control-Max-Age', Cors.maxAge)
    }
    if (Cors.exposedHeaders && Cors.exposedHeaders != '') {
      res.header('Access-Control-Expose-Headers', Cors.exposedHeaders)
    }
    if (Cors.credentials && Cors.credentials != '') {
      res.header('Access-Control-Allow-Credentials', Cors.credentials)
    }
    next()

  })

  const routing = function (ep) {
    for (var i = 0; i < ep.length; i++) {
      if (ep[i].methods.length == 0) {
        Logger.error('No Methods: Please put something like ["GET"] or ["GET","POST","PUT"]')
        return false;
      }



      const get = ep[i].methods.indexOf("GET")
      const post = ep[i].methods.indexOf("POST")
      const put = ep[i].methods.indexOf("PUT")
      const del = ep[i].methods.indexOf("DELETE")
      const patch = ep[i].methods.indexOf("PATCH")
      const multipart = (ep[i].type == "multipart")

      const route = router.route(ep[i].url)
      if (get > -1) {
        route.get((ep[i].middleware && ep[i].middleware.length > 0) ? ep[i].middleware : ep[i].operator, ep[i].operator)
        Logger.endpoint(`[ GET ] \t\t\t\t=> ${config.api.prefix}${ep[i].url}`)
      }
      if (post > -1) {
        if (multipart) {
          route.post(upload.single('filename'), ep[i].operator)
          Logger.endpoint(`[ POST ] @ Multipart \t\t=> ${config.api.prefix}${ep[i].url}`)
        } else {
          route.post((ep[i].middleware && ep[i].middleware.length > 0) ? ep[i].middleware : ep[i].operator, ep[i].operator)
          Logger.endpoint(`[ POST ] \t\t\t\t=> ${config.api.prefix}${ep[i].url}`)
        }
      }
      if (put > -1) {
        route.put((ep[i].middleware && ep[i].middleware.length > 0) ? ep[i].middleware : ep[i].operator, ep[i].operator)
        Logger.endpoint(`[ PUT ] \t\t\t\t=> ${config.api.prefix}${ep[i].url}`)
      }
      if (del > -1) {
        route.delete((ep[i].middleware && ep[i].middleware.length > 0) ? ep[i].middleware : ep[i].operator, ep[i].operator)
        Logger.endpoint(`[ DELETE ] \t\t\t=> ${config.api.prefix}${ep[i].url}`)
      }
      if (patch > -1) {
        route.patch((ep[i].middleware && ep[i].middleware.length > 0) ? ep[i].middleware : ep[i].operator, ep[i].operator)
        Logger.endpoint(`[ PATCH ] \t\t\t\t=> ${config.api.prefix}${ep[i].url}`)
      }
    }
    console.log('\n\n\n----------------------------------------------------------------\n')
  }

  // BODY PARSER
  app.use(bodyParser.urlencoded({
      extended: true,
      limit: '70mb'
    }))
    .use(bodyParser.json({
      limit: '70mb'
    }))

  // PREFIX FOR UPLOAD
  app.use(config.api.prefix, router)

  //PREFIX FOR SERVICE PUBLIC FILES
  app.use(publicRoute, express.static(publicFolder))

  if (!config.ssl || config.ssl === false) {
    HTTP.createServer(app)
      .listen(config.ports.http)
    console.log('\n\n\n================================================================\n')
    Logger.success('SERVER STARTED')
    Logger.success('VERSION : 1.0.35')
    Logger.success('PROTOCOL : HTTP')
    Logger.success('ON PORT : ' + config.ports.http)

    if (!Cors) return routing

    if (Cors.origin && Cors.origin != '') {
      Logger.header('Cors Origin:' + Cors.origin)
    }
    if (Cors.methods && Cors.methods != '') {
      Logger.header('Cors Methods:' + Cors.methods)
    }
    if (Cors.headers && Cors.headers != '') {
      Logger.header('Cors Headers:' + Cors.headers)
    }
    if (Cors.maxAge && Cors.maxAge != '') {
      Logger.header('Cors Max Age:' + Cors.maxAge)
    }
    if (Cors.exposedHeaders && Cors.exposedHeaders != '') {
      Logger.header('Cors Exposed Headers:' + Cors.exposedHeaders)
    }
    if (Cors.credentials && Cors.credentials != '') {
      Logger.header('Cors Credentials:' + Cors.credentials)
    }
    console.log('\n================================================================\n\n\n')
    return routing

  } else if (config.ssl === true) {

    const privateKey = fs.readFileSync(config.keys.privateKey, 'utf8')
    const certificate = fs.readFileSync(config.keys.certificate, 'utf8')
    const cred = {
      key: privateKey,
      cert: certificate
    };
    HTTPS.createServer(cred, app)
      .listen(config.ports.https)
    console.log('\n\n\n================================================================\n')
    Logger.success('SERVER STARTED')
    Logger.success('VERSION : 1.0.35')
    Logger.success('PROTOCOL : HTTP')
    Logger.success('ON PORT : ' + config.ports.http)

    if (!Cors) return routing

    if (Cors.origin && Cors.origin != '') {
      Logger.header('Cors Origin:' + Cors.origin)
    }
    if (Cors.methods && Cors.methods != '') {
      Logger.header('Cors Methods:' + Cors.methods)
    }
    if (Cors.headers && Cors.headers != '') {
      Logger.header('Cors Headers:' + Cors.headers)
    }
    if (Cors.maxAge && Cors.maxAge != '') {
      Logger.header('Cors Max Age:' + Cors.maxAge)
    }
    if (Cors.exposedHeaders && Cors.exposedHeaders != '') {
      Logger.header('Cors Exposed Headers:' + Cors.exposedHeaders)
    }
    if (Cors.credentials && Cors.credentials != '') {
      Logger.header('Cors Credentials:' + Cors.credentials)
    }
    console.log('\n================================================================\n\n\n')
    return routing
  }

}

export default Server