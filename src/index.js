import Server from './libs/server'
import Store from './libs/store'
import { External_Logger, Colorize } from './utils/logger'
import LoopNext from 'loopnext'

const HEX = function (config) {
  this.Route = new Server(config)
  this.Log = Colorize(config)
  this.Iterate = new LoopNext().syncLoop
  if (config.redis) {
    new Store(config)
      .then((stores) => {
        this.Store = stores
        // return this
      })
  } else {
    // return this
  }
}

module.exports = HEX
